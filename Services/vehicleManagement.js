const config = require("../Utilities/config").config;
const vehicleDAO = require('../DAO/vehicleDAO');
const MD5 = require('md5');
const Vehicle = require("../Models/Vehicule");


/**API for Vehicle */
//Add


exports.Create = (req, res) => {
    
    if (!req.body) {
        return res.status(400).json({
            result: null,
            code: 0,
            message: "Note content can not be empty"
        });
    }


    const vehicle = new Vehicle({
        marque: req.body.marque,
        category: req.body.category,
        referenceSensor: req.body.referenceSensor,
        fuelLevel: req.body.fuelLevel,
        speed: req.body.speed,
        coordinates: req.body.coordinates,
        driver: req.body.driver,
        registration_number: req.body.registration_number

       

    });





    vehicle.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).json({
                
                code: 0,
                message: err.message || "Some error occurred while creating the Note.",
                result: null
            });
        });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {

    Vehicle.find()
        .then(vehicles => {
            return res.status(200).json({ result: vehicles, code: 0, message: null });
        }).catch(err => {
            return res.status(500).send({
                result: null,
                code: 1000,
                message: err.message || "Some error occurred while retrieving notes."
            });
        });


};

// Find a single vehicle with a vehicleId
exports.findOne = (req, res) => {

    Vehicle.findById(req.params.vehicleId)
        .then(vehicle => {
            if (!vehicle) {
                return res.status(404).json({
                    message: "vehicle not found with ID" + req.params.vehicleId,
                    result: null,
                    code: 1000
                });

            }
            return res.json({
                result: vehicle,
                code: 0,
                message: null
            });
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).json({
                    result: null,
                    code: 0,
                    message: "vehicle not found with ID" + req.params.vehicleId
                });
            }
            return res.status(500).json({
                result: null,
                code: 0,
                message: "Error retrieving note with id " + req.params.vehicleId
            });
        });


};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {

    // Validate Request
    if (!req.body) {
        return res.status(400).json({
            result: null,
            code: 0,
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Vehicle.findByIdAndUpdate(req.params.vehicleId, {
        marque: req.body.marque,
        category: req.body.category,
        referenceSensor: req.body.referenceSensor,
        fuelLevel: req.body.fuelLevel,
        speed: req.body.speed,
        coordinates: req.body.coordinates,
        driver: req.body.driver,
        registration_number: req.body.registration_number




    }, { new: true })
        .then(vehicle => {

            if (!vehicle) {
                return res.status(404).json({
                    result: null,
                    code: 0,
                    message: "vehicle not found with id" + req.params.vehicleId
                });
            }
            return res.json({
                result: vehicle,
                code: 0,
                message: null
            });

        }).catch(err => {

            if (err.kind === 'ObejectId') {

                return res.status(404).json({
                    result: null,
                    code: 0,
                    message: "vehicle not found with id" + req.params.vehicleId
                });
            }
            return res.status(500).json({
                result: null,
                code: 1000,
                message: "Error updating vehicle with id " + req.params.vehicleId
            });

        });

};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {

    Vehicle.findByIdAndRemove(req.params.vehicleId)
        .then(vehicle => {

            if (!vehicle) {
                return res.status(404).json({
                    code: 1001,
                    message: "vehicel note found with id" + req.params.vehicleId,
                    result: null
                });
            }
            return res.status(200).json({});
        }).catch(err => {

            if (err.kind === 'ObjectId' || err.marque === 'NotFound') {
                return res.status(404).json({
                    code: 1001,
                    message: "vehicel note found with id" + req.params.vehicleId,
                    result: null
                });
            }
            return res.status(500).json({
                code: 1001,
                result: null,
                message: "Could not delete vehicle with id " + req.params.vehicleId
            });
        });
};



