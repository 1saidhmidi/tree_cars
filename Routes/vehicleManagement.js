const express = require("express");
const router = express.Router({ mergeParams: true });
const vehicleService = require("../Services/vehicleManagement");



    // Create a new vehicle
    /*app.post('/vehicle',function(req,res){
        vehicleService.Create
    } );
*/
    router.post('/vehicles',vehicleService.Create);

    // Retrieve all vehicle
    router.get('/vehicles', vehicleService.findAll);

    // Retrieve a single vehicle with vehicleId
    router.get('/vehicles/:vehicleId', vehicleService.findOne);

    // Update a vehicle with vehicleId
    router.put('/vehicles/:vehicleId', vehicleService.update);

    // Delete a vehicle with vehicleId
    router.delete('/vehicles/:vehicleId', vehicleService.delete);
    
    module.exports = router;
