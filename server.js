/**
 * Module dependencies.
 */
const express = require('express');
const app = express();
const path = require('path');
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const cors = require('cors');
var User = require('./Models/User');
var Vehicle=require('./Models/Vehicule');
var debug = require('debug');
var http = require('http');
// var mongoose1 = require('mongoose');

const mongoose = require('./Utilities/mongooseConfig')();

const authRoute = require('./Routes/auth');
const vehicleRoute = require('./Routes/vehicleManagement');
const config = require("./Utilities/config").config;
const MaileRouter = require("./Routes/maile");
app.use(express.static(path.join(__dirname, '/dist/')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));



app.use(cors({origin:"*"}));

app.use((err, req, res, next) => {
  return res.send({
    "statusCode": 401,
    "statusMessage": "Something Went Wrong!"
  });
});

app.use('/auth', authRoute);
app.use('',vehicleRoute);
app.use('',MaileRouter);
// catch 404 and forward to error handler
app.use((req, res, next) => {
  next();
});
//////












///nodemailer
/*
app.post('/sendemail',function(req,res){

let transporter = nodemailer.createTransport({
       service : 'gmail',
       secure : false,
       port : 25,
       auth: {

             user:'saidhmidi96@gmail.com',
              pass: 'SAIDhmidi1996jj'
               
       },
       tls:{

        rejectUnauthorized: false
       }


});
let HelperOptions = {
       from :'"TreeTronix" <saidhmidi96@gmail.com>',
        to:'said.hmidi@esprit.tn',
        subject:'hello world',
        text:'ca marche',
        attachments:[{
          filname:'123.jpg', path:'./123.jpg'
        }]

};
transporter.sendMail(HelperOptions,(error,info)=>{
if(error){
  res.json({status: 'error'});

}

res.json({status: 'success'});
console.log(info);
});
});

*/


// app.get('*', function (req, res) {
//   res.sendFile(path.join(__dirname + '/dist/index.html'));
// });

/**
 * Start Express server.
 */
server.listen(config.NODE_SERVER_PORT.port, () => {
  console.log('app listening on port:' + config.NODE_SERVER_PORT.port);
});
