var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let VehicleSchema = new Schema({
id:{
type:String
},
marque:{
    type: String,
    trim: true,
    default: null,
    required: true
},
category:{
    type: String,
    trim: true,
    default: null,
    required: false
},
referenceSensor:{

    type: String,
    trim: true,
    default: null,
    required: true
},

fuelLevel:{
    type: String,
    trim: true,
    default: null,
    required: false

},
speed:{
    type: String,
    trim: true,
    default: null,
    required: false

},
coordinates:{

    lat: Number,
    lng: Number,
    required:false
},
driver:{
    type: String,
    trim: true,
    default: null,
    required: true
},
registration_number:{

    type: String,
    trim: true,
    default: null,
    required: true
}


});
module.exports = mongoose.model('Vehicle', VehicleSchema);